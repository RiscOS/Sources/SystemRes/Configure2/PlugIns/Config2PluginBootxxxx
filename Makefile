# Copyright 1998 Acorn Computers Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Makefile for BootApps / BootBoot / BootRun
#

COMPONENT ?= BootRun
override TARGET = !RunImage
INSTTYPE   = app
OBJS       = List Main ToolboxE WimpE WimpM
LIBS       = ${CONLIB} ${EVENTLIB} ${TBOXLIB} ${WIMPLIB}
CINCLUDES  = ${CONINC} ${TBOXINC}
INSTAPP_DEPENDS = localecopy
INSTAPP_FILES = ${COMPONENT}.!Boot !Help !Run !RunImage Messages Res \
                ${COMPONENT}.!Sprites ${COMPONENT}.!Sprites22 ${COMPONENT}.!Sprites11 \
                ${COMPONENT}.Ursula.!Sprites:Ursula \
                ${COMPONENT}.CoSprite ${COMPONENT}.CoSprite22 ${COMPONENT}.CoSprite11 \
                ${COMPONENT}.Ursula.CoSprite:Ursula
INSTAPP_VERSION = Messages

ifeq (${COMPONENT},Config2PluginBootxxxx)
# The Continuous Integration system calls the Makefile with the name of the
# top level component, redefine it to one of the three valid xxxxMerge values.
COMPONENT := BootRun
endif
ifeq (${COMPONENT},BootApps)
CDEFINES   = -DAPP=1
endif
ifeq (${COMPONENT},BootBoot)
CDEFINES   = -DAPP=2
endif
ifeq (${COMPONENT},BootRun)
CDEFINES   = -DAPP=3
endif

include CApp

C_WARNINGS = -fa

# Copy the locale specific files up a level to Resources so that the search
# path and uses of LocalRes: in the shared makefiles can find them.
# The non-locale specific files are explicitly referenced by component name
# in the list of files above.
localecopy:
	${CP} Resources${SEP}${COMPONENT}${SEP}${LOCALE} Resources${SEP}${LOCALE} ${CPFLAGS}

clean::
	${XWIPE} Resources${SEP}${LOCALE} ${WFLAGS}

# Dynamic dependencies:
